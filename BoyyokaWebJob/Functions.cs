﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace BoyyokaWebJob
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        {
            log.WriteLine(message);
        }

        private static void UpdateFeaturedIndex()
        {
            Random rnd = new Random();
            int id = rnd.Next(1, 21);
            string connectionString = ConfigurationManager.ConnectionStrings["BoyyoyoContext"].ConnectionString;
            var conn = new SqlConnection(connectionString);
            var cmd = conn.CreateCommand();

            try
            {
                cmd.CommandText = "UPDATE BoyyokaGlobal SET FeaturedIndexId = @id";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                conn.Open();
                cmd.Prepare();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                string fileName = "DatabaseLog.txt";
                string filePath = AppDomain.CurrentDomain.BaseDirectory + fileName;

                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                       "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                string fileName = "ServiceLog.txt";
                string filePath = AppDomain.CurrentDomain.BaseDirectory + fileName;

                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                       "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                }
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
                conn.Dispose();
            }
        }
    }
}
