﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Boyyoka.Models;

namespace Boyyoka.Helpers
{
    /*http://stackoverflow.com/questions/5377049/entity-framework-avoiding-inserting-duplicates*/
    public class DuplicateHelper
    {
        /*maybe it's not in the database yet, but it's awaiting insertion*/
        public static Tag GetTag(string tagName, BoyyoyoContext context, User user) 
        {       
            var tagInDatabase = context.Tags.FirstOrDefault(t => t.TagName == tagName);
            if(tagInDatabase != null)
            {             
                context.Tags.Attach(tagInDatabase);
                tagInDatabase.Tagged++;
                return tagInDatabase;
            }
            else
            {
                var manager = ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager;
                var tagInCache = manager.GetObjectStateEntries(EntityState.Added)
                        .Where(ose => ose.EntityKey.EntitySetName.Equals("Tags"))
                        .Select(ose => ose.Entity)
                        .Cast<Tag>()
                        .Where(ose => ose.TagName == tagName)
                        .FirstOrDefault();                
                if (tagInCache != null)
                {
                    tagInCache.Tagged++;
                    return tagInCache;
                }
                else
                {
                    var newTag = new Tag()
                    {
                        User = user,
                        TagName = tagName,
                        Tagged = 1
                    };
                    context.Entry(newTag).State = System.Data.Entity.EntityState.Added;
                    return newTag;
                }            
            }
        }      
    }
}