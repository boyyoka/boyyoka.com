﻿using System;
using System.Web.Mvc;
using Boyyoka.Helpers;
using Boyyoka.Services;
using Boyyoka.ViewModels;
using Boyyoka.Models.Interfaces;
using Boyyoka.Interfaces;

namespace Boyyoka.Controllers
{
    public class UserController : Controller
    {

        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: /User/
        public ActionResult Signin(string returnUrl)
        {
            if(!String.IsNullOrEmpty(SessionManager.SessionManager.getString("uname")))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
        }
     
        public ActionResult Register(string returnUrl)
        {
            if (!String.IsNullOrEmpty(SessionManager.SessionManager.getString("uname")))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
        }

        public ActionResult Profile()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult Upload()
        {
            return View();      
        }
     
        public ActionResult Signout()
        {
            if (!_userService.SignoutUser())
            {
                ViewBag.Error("Failed to signout, you're probably not even logged in! cheater!");
            }
            return Redirect(Request.UrlReferrer.AbsoluteUri);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Signin(SigninViewModel model) 
        {
            if(ModelState.IsValid)
            {           
                if (_userService.SigninUser(model))
                {
                    string returnUrl = model.ReturnUrl;                
                    ModelState.Clear();
                    model = null;
                    if(returnUrl.Length > 0)
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }                           
                }
                else 
                {
                    ModelState.AddModelError("", "Incorrect username or password");
                }                                          
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model) 
        {
            if(ModelState.IsValid)
            {
                if (_userService.RegisterNewUser(model))
                {
                    //refresh the new user list.
                    GlobalData.setNewUsers();

                    string returnUrl = model.ReturnUrl;
                    
                    SigninViewModel login = new SigninViewModel();
                    login.Username = model.Username;
                    login.Password = model.Password;
                    if (_userService.SigninUser(login))
                    {
                        ModelState.Clear();
                        model = null;
                        if (returnUrl != null && returnUrl.Length > 0)
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            ViewBag.Message = "Welcome to the pit of the interwebz!!!";
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else 
                    {
                        ViewBag.Message = "Failed to sign in as a new user, try again shortly!";
                        return RedirectToAction("Index", "Home");
                    }                  
                    
                }
                else 
                {
                    ViewBag.Message = model.ErrorMessage;
                    return View();
                }                              
            }
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Upload")]
        public ActionResult UploadWallpaper()
        {         
            var _request = Request;
            _userService.UploadWallpaper(_request);
            return View();
        }
    }
}