﻿using Boyyoka.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boyyoka.ViewModels
{
    public class WallpaperViewModel
    {
        public int WallpaperID { get; set; }
        public string Uploader { get; set; }
        public DateTime Date { get; set; }
        public ICollection<Tag> Tags { get; set; }
        public string Resolution { get; set; }
        public string ImagePath { get; set; }
        public string ImageId { get; set; }
        public string ThumbnailPath { get; set; }
        public int Views { get; set; }
        public int Favorites { get; set; }
        public string Purity { get; set; }
        public string Type { get; set; }
        public bool isFeatured { get; set; }

        public WallpaperViewModel()
        {
            Tags = new HashSet<Tag>();
        }
        
    }
}