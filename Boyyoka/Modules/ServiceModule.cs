﻿using Autofac;
using Boyyoka.Models.Interfaces;
using Boyyoka.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Boyyoka.Modules
{
    public class ServiceModule : Autofac.Module
    {
        //will register every class the ends with “Service” in Autofac.
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}