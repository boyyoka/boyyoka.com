﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Boyyoka.Helpers
{
    /*http://stackoverflow.com/questions/1498727/asp-net-mvc-how-to-show-unauthorized-error-on-login-page*/
    public class CustomAuthorize : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (SessionManager.SessionManager.getString("uname") != null && SessionManager.SessionManager.getString("uname").Length == 0)
            {   
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new 
                        { 
                            controller = "User", 
                            action = "Signin",
                            returnUrl = filterContext.HttpContext.Request.Url.AbsoluteUri
                        }
                        ));
            }
        }
    }
}