﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boyyoka.ViewModels
{
    public class ViewModelWrapper
    {
        public SearchViewModel SearchViewModel { get; set; }
        public RegisterViewModel RegisterViewModel { get; set; }
        public SigninViewModel SigninViewModel { get; set; }
        public PageViewModel PageViewModel { get; set; }
        //for loading details of a wallpapers
        public WallpaperViewModel WallpaperViewModel { get; set; }
        //for loading a set of wallpapers
        public List<WallpaperViewModel> WallpapersViewModel { get; set; }
        public List<TagViewModel> TagsViewModel { get; set; }
        public List<UserViewModel> NewUsers { get; set; }
    }
}